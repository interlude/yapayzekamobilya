﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace YapayZekaMobilya
{
    class SquareTable
    {
        public SquareTable(float width, float limit, float x, float y){
            this.width = width;
            this.limit = limit;
            this.x = x;
            this.y = y;

            Rect = new Rectangle((int)(x - width), (int)(y - width),(int) width, (int)width);
            ShiftedRectangle = new Rectangle();
        }

        Rectangle Rect;
        Rectangle ShiftedRectangle;
        public Rectangle GetRect()
        {
            return Rect;
        }

        public Rectangle GetRect(int minX, int minY)
        {

            ShiftedRectangle= Rect;
            ShiftedRectangle.X += minX;
            ShiftedRectangle.Y += minY;
            return ShiftedRectangle;
        }
        float x;
        float y;
        float limit;
        float width;
    }

}
