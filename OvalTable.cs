﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace YapayZekaMobilya
{
    class OvalTable
    {
        public OvalTable(float diameter, float limit, float x, float y){
            this.diameter = diameter;
            this.limit = limit;
            this.x = x;
            this.y = y;

            Rect = new Rectangle((int)(x - diameter), (int)(y - diameter),(int) diameter*2, (int)diameter*2);
            ShiftedRectangle = new Rectangle();
        }

        Rectangle Rect;
        Rectangle ShiftedRectangle;
        public Rectangle GetRect()
        {
            return Rect;
        }

        public Rectangle GetRect(int minX, int minY)
        {

            ShiftedRectangle= Rect;
            ShiftedRectangle.X += minX;
            ShiftedRectangle.Y += minY;
            return ShiftedRectangle;
        }
        float x;
        float y;
        float diameter;
        float limit;
    }
}
