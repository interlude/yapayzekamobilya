﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;


namespace YapayZekaMobilya
{
    public partial class MainWindow : Form
    {
        Rectangle RoomRectangle;



        Pen RoomPen = new Pen(Color.Black);
        Pen TablePen = new Pen(Color.Red);
        Pen ChairPen = new Pen(Color.Green);
        Rectangle DrawArea;


        Boolean init = false;
        public void InitGraphicParameters()
        {
            RoomRectangle = new Rectangle(3, 3, 300, 300);
            

            init = true;

        }
        public void MainWindow_Paint(Object sender,
    PaintEventArgs e)
        {
            if (init)
            {
                Graphics GraphicsObject = e.Graphics;
                PrintGuideBox(e, RoomRectangle);
                
                PrintOvalTable(e, new OvalTable(2, 3, 0, 0));
                PrintSquareTable(e, new SquareTable(5,5,10,10));
            }


        }

        private void PrintGuideBox(PaintEventArgs e, Rectangle rectangle)
        {
            
            e.Graphics.DrawRectangle(RoomPen, RoomRectangle);

        }


        private void PrintOvalTable(PaintEventArgs e, OvalTable ovalTable)
        {
            Graphics GraphicsObject = e.Graphics;
            
            DrawArea = ovalTable.GetRect(RoomRectangle.X, RoomRectangle.Y);
            GraphicsObject.DrawEllipse(TablePen, DrawArea);

        }

        private void PrintSquareTable(PaintEventArgs e, SquareTable squareTable)
        {
            Graphics GraphicsObject = e.Graphics;

            DrawArea = squareTable.GetRect(RoomRectangle.X, RoomRectangle.Y);
            GraphicsObject.DrawRectangle(TablePen, DrawArea);
        }

    }
}
